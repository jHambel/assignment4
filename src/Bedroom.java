//This us my project

/**
 * 
 * @author jjh20130
 * Assignmet4- Tanes CSC200
 * 
 * Purpose: Create a class for a room in a house, have attributes of the 
 * room and be able to get and set them.
 * Have a toString() method which outputs all attributes in the class.
 *
 */
public class Bedroom {

	/** 
	 * Declaring the Attributes of the class
	 * 
	 * Wall color 0- Red, 1- Blue, 2- Green
	 * Wood type  0- Hard wood, 1- Carpeting, 2- Tile
	 * Windows is just the number of windows
	**/ 
	
	//YOLO
	int wallColor, floorType, windows;

	/**
	 * Public Constructor, sets all attributes of the class to the defualts
	 */
	//Public Constructor
	public Bedroom(){

		wallColor= 0;
		floorType= 0;
		windows= 0;
	}
	
	/**
	 * Public Constructor, sets all attributes to their declared values
	 * @param wall
	 * @param floor
	 * @param win
	 */
	public Bedroom(int wall, int floor, int win){

		wallColor= wall;
		floorType= floor;
		windows= win;
	}

	/**
	 * Outputs all attributes to console
	 * @return the attributes of room
	 */
	//Outputs values of all attributes
	@Override
	public String toString(){

		if(wallColor == 0){
			
			if(floorType ==0){

				return "The color of the walls is red. The type of floor is hard wood."
						+ " There are " + windows + " windows in the room.";
			}else if(floorType == 1){

				return "The color of the walls is red. The type of floor is carpeting."
						+ " There are " + windows + " windows in the room.";
			}else{

				return "The color of the walls is red. The type of floor is Tile."
						+ " There are " + windows + " windows in the room.";
			}
		}else if(wallColor == 1){

			if(floorType ==0){

				return "The color of the walls is blue. The type of floor is hard wood."
						+ " There are " + windows + " windows in the room.";
			}else if(floorType == 1){

				return "The color of the walls is blue. The type of floor is carpeting."
						+ " There are " + windows + " windows in the room.";
			}else{

				return "The color of the walls is blue. The type of floor is Tile."
						+ " There are " + windows + " windows in the room.";
			}
		}else{
			if(floorType ==0){

				return "The color of the walls is green. The type of floor is hard wood."
						+ " There are " + windows + " windows in the room.";
			}else if(floorType == 1){

				return "The color of the walls is green. The type of floor is carpeting."
						+ " There are " + windows + " windows in the room.";
			}else{

				return "The color of the walls is green. The type of floor is hard wood."
						+ " There are " + windows + " windows in the room.";
			}
		}
	}

	/**
	 * Returns the color of the wall
	 * @return wallColor
	 */
	//Returns the color of the wall
	public int getWallColor(){

		return wallColor;
	}

	/**
	 * Returns the type of the floor
	 * @return floorType
	 */
	//Returns the type of floor
	public int getFloorType(){

		return floorType;
	}

	/**
	 * Returns the number of windows
	 * @return windows
	 */
	//Returns the number of windows
	public int getWindow(){

		return windows;
	}

	/**
	 * Sets the color of the wall to the given value
	 * @param temp
	 */
	//Sets the color of the wall
	public void setWallColor(int temp){

		wallColor=temp;
	}

	/**
	 * Sets the type of the floor to the given value
	 * @param temp
	 */
	//Sets the type of the floor
	public void setFloorType(int temp){

		floorType=temp;
	}

	/**
	 * Sets the number of windows
	 * @param temp
	 */
	//Sets the number of windows
	public void setWindows(int temp){

		windows=temp;
	}
	
	/**
	 * Main of the program
	 */
	//For Testing Purposes
	public static void main(String args[]){

		Bedroom bed= new Bedroom(0,0,0);
		System.out.println(bed.toString());

	}
}